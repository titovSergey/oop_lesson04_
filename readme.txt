Домашнее задание 5
Добавлено: 06.01.2020 18:52

Классы для работы с Деньгами



Создать класс Currency с приватным свойством isoCode.

1
Название валюты передавать в формате ISO 4217 (USD, EUR, ...)
через конструктор класса.

2
Для свойства isoCode написать гэттер и сеттер.

3
В случае неверного формата названия валюты выбросить InvalidArgumentException.

4
Реализовать публичный метод equals для сравнения двух валют на равенство.



Создать класс Money с приватным свойствами:
- amount (int|float) и
- currency (Currency).

1
Число и валюту принимать через конструктор класса.

2
Для свойств amount и currency написать гэттеры и сеттеры.

3
Реализовать публичный метод equals для сравнения двух
денежных единиц по числу и валюте.

4
Реализовать метода add (для суммирования денежных единиц),который будет принимать объект Money.
Если валюта добавляемого объекта отличается, то выбросить InvalidArgumentException.



sudo a2dismod php7.2
sudo a2enmod php7.4
sudo service apache2 restart




//
//        switch (strtolower($currencyName)) {
//            case 'usd':
//                $this->isoCode = 840;
//                break;
//            case 'rur':
//                $this->isoCode = 810;
//                break;
//            case 'eur':
//                $this->isoCode = 978;
//                break;
//            case 'uah':
//                $this->isoCode = 890;
//                break;
//            default:
//                throw new InvalidArgumentException('invalid currency name: '.$currencyName);
//        }



//        if ($this->currency->getCode() == $money->currency->getCode()) {
//            //echo $this->currency;
//            echo $this->amount + $money->amount.' '.$this->currency->getCurrencyName();
//        }
//        else{
//            throw new InvalidArgumentException('currency types are different');
//            echo "currency types are different";
//        }