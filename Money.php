<?php


class Money
{
    private int $amount;
    private Currency $currency;


    public function __construct($amount, $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function equals($money)
    {
        if ($this->currency->getCode() == $money->currency->getCode()) {
            echo 'the same currency';
        }
        else{
            echo "can't be compared";
        }
    }

    public function add($money)
    {
        if($this->currency->equals($money->currency)) {
            echo $this->amount + $money->amount.' '.$this->currency->getCurrencyName();
        } else {
            throw new InvalidArgumentException('currency types are different');
        }
    }
}