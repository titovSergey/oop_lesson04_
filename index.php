<?php


require_once 'Money.php';
require_once 'Currency.php';


$USD = new Currency('usd');
$EUR = new Currency('eur');
$UAH = new Currency('uah');
$RUR = new Currency('rur');


$curr = new Currency('rur');
echo $curr->getCode();
echo '<br>';

$curr->setCode(1000);
echo $curr->getCode();
echo '<br>';

echo $USD->getCode();
echo '<br>';

echo '<br>';
echo $EUR->getCode();
echo '<br>';
echo $UAH->getCode();
echo '<br>';
echo $RUR->getCode();
echo '<br>';
$USD->equals($EUR);
echo '<br>';
$EUR->equals($EUR);
echo '<br>';
$UAH->equals($EUR);
echo '<br>';
echo '<br>';
echo '<br>';



try {
    $money_1 = new Money(5500,new Currency('usd'));
    $money_2 = new Money(300, new Currency('rur'));
    $money_3 = new Money(1500, new Currency('uah'));
    $money_4 = new Money(2500, new Currency('usd'));
    $money_5 = new Money(400, new Currency('usd'));
    $money_6 = new Money(2500, new Currency('uah'));
    echo '<br>';
    $money_1->equals($money_2);
    echo '<br>';
    $money_1->equals($money_3);
    echo '<br>';
    $money_1->equals($money_4);
    echo '<br>';
    $money_1->equals($money_5);
    echo '<br>';

    echo '<br>';
    $money_1->add($money_5);
    echo '<br>';
    echo '<br>';
    $money_1->add($money_3);
    echo '<br>';

} catch (InvalidArgumentException $exception) {
    echo '-----   '. $exception->getMessage();
}





