<?php


class Currency
{
    private int $isoCode;

    private $allCurrencies = [
        'usd'=>840,
        'rur'=>810,
        'eur'=>978,
        'uah'=>890
    ];

    public function __construct($currencyName)
    {

        if (isset($this->allCurrencies[$currencyName])) {
            $this->isoCode = $this->allCurrencies[$currencyName];
        } else {
            throw new InvalidArgumentException('invalid currency name: '.$currencyName);
        }


    }

    public function getCode(){
        return $this->isoCode;
    }

    public function setCode($code){
        if (in_array($code, array_values($this->allCurrencies))) {
            $this->isoCode = $code;
        } else {
            echo 'invalid code';
        }
    }

    public function equals($currency){
        if ($this->isoCode == $currency->isoCode){
//            echo 'currency types are the same';
            return true;
        } else {
//            echo 'currency types are not the same';
            return false;
        }
    }

    // returns usd || eur || ...
    public function getCurrencyName () {
        $code = $this->getCode();
        $item = array_search($code, $this->allCurrencies);
        return $item;
    }

}